package nxtgizmo.signalling.addcontact;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import nxtgizmo.signalling.sinallingapp.SignallingApp;
import butterknife.BindView;
import butterknife.ButterKnife;
import nxtgizmo.signalling.R;

public class AddContactActivity extends AppCompatActivity implements AddContactContract{

    @Inject
    SharedPreferences sharedPreferences;

    AddContactPresenter addContactPresenter;

    @BindView(R.id.aa)
    TextView edtContactNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((SignallingApp)getApplication()).getContactComponent().inject(this);
        ButterKnife.bind(this);
        addContactPresenter = new AddContactPresenter(this);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onSuccess(String successMessage) {
        Toast.makeText(AddContactActivity.this, successMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText(AddContactActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
    }
}
