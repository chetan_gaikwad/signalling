package nxtgizmo.signalling.addcontact;

/**
 * Created by chetan_g on 22/11/16.
 */

interface AddContactContract {
     void onSuccess(String successMessage);
     void onError(String errorMessage);
}
