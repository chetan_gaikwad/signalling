package nxtgizmo.signalling.dashboard;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;

import java.util.List;

import javax.inject.Inject;

import nxtgizmo.signalling.R;
import nxtgizmo.signalling.sinallingapp.SignallingApp;
import butterknife.ButterKnife;
import retrofit2.Retrofit;
import timber.log.Timber;

public class DashBoardActivity extends AppCompatActivity implements DashboardContract{

    @Inject
    MqttAndroidClient client;
    @Inject
    SharedPreferences preferences;
    @Inject
    LocationManager locationManager;
    @Inject
    Retrofit retrofit;

    private double mLatitude;
    private double mLongitude;

    private int LOCATION_PERMISSION_CODE = 23;

    private DashboardPresenter dashboardPresenter;

    private SensorManager sensorManager;
    private Sensor orientSensor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((SignallingApp)getApplication()).getContactComponent().inject(this);
        ButterKnife.bind(this);
        dashboardPresenter = new DashboardPresenter(this);
        dashboardPresenter.connectToMqtt(client);
        if (isLocationAllowed(Manifest.permission.ACCESS_FINE_LOCATION)) {
            dashboardPresenter.setUpLocation(locationManager);
        } else {
            requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, LOCATION_PERMISSION_CODE);
        }

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        // get Orientation sensor
        List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ORIENTATION);
        if (sensors.size() > 0)
            orientSensor = sensors.get(0);

        sensorManager.registerListener(orientListener, orientSensor,
                SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onSuccess(String successMessage) {

    }

    @Override
    public void onError(String errorMessage) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        dashboardPresenter.getRepos(retrofit);
    }

    @Override
    public void onLocationUpdate(Location location) {
        Timber.d("Location changed %f - %f - %f",location.getLatitude(), location.getLongitude(), location.getBearing());
        mLatitude = location.getLatitude();
        mLongitude = location.getLongitude();
    }

    /**
     * Generic method to check permission status
     * @param permissionName
     * @return
     */
    private boolean isLocationAllowed(String permissionName) {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, permissionName);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    /**
     * Generic method to request permission
     * @param permissionName
     * @param requestCode
     */
    private void requestPermission(String permissionName, int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,permissionName)){
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
            Snackbar
                    .make(findViewById(android.R.id.content), "Location permission was denied " +
                            "earlier, Please enable it from app settings", Snackbar.LENGTH_LONG)
                    .show();

        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(this,new String[]{permissionName},requestCode);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if(requestCode == LOCATION_PERMISSION_CODE){

            //If permission is granted
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //Displaying a toast
                Toast.makeText(this,"Permission granted for location",Toast.LENGTH_LONG).show();
                dashboardPresenter.setUpLocation(locationManager);
            }else{
                //Displaying another toast if permission is not granted
                Toast.makeText(this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        }
    }

    private final SensorEventListener orientListener = new SensorEventListener() {

        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0];
            String dir = "";
            if (x >= 337.5 || x < 22.5) {
                dir = "N";
            } else if (x >= 22.5 && x < 67.5) {
                dir = "NE";
            } else if (x >= 67.5 && x < 112.5) {
                dir = "E";
            } else if (x >= 112.5 && x < 157.5) {
                dir = "SE";
            } else if (x >= 157.5 && x < 202.5) {
                dir = "S";
            } else if (x >= 202.5 && x < 247.5) {
                dir = "SW";
            } else if (x >= 247.5 && x < 292.5) {
                dir = "W";
            } else if (x >= 292.5 && x < 337.5) {
                dir = "NW";
            }
            Timber.d("Direction %s",dir);
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // do nothing
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dashboardPresenter.unSubscribeMqttChannel(client);
        dashboardPresenter.disconnectMqtt(client);
    }
}
