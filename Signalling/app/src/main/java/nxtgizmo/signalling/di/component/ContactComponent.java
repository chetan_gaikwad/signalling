package nxtgizmo.signalling.di.component;



import javax.inject.Singleton;

import nxtgizmo.signalling.addcontact.AddContactActivity;
import dagger.Component;
import nxtgizmo.signalling.di.model.AppModule;
import nxtgizmo.signalling.dashboard.DashBoardActivity;
import nxtgizmo.signalling.di.model.NetModule;


@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface ContactComponent {
   void inject(AddContactActivity addContactActivity);
   void inject(DashBoardActivity dashBoardActivity);
}