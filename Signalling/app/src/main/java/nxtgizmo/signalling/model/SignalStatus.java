package nxtgizmo.signalling.model;

public class SignalStatus
{
    private String time;

    private String st;

    public String getTime ()
    {
        return time;
    }

    public void setTime (String time)
    {
        this.time = time;
    }

    public String getSt ()
    {
        return st;
    }

    public void setSt (String st)
    {
        this.st = st;
    }

    @Override
    public String toString()
    {
        return "SignalStatus [time = "+time+", st = "+st+"]";
    }
}