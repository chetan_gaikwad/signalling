package nxtgizmo.signalling.services;


import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface GitHubService {
  @GET("/posts/{post_id}")
  Observable<String> listRepos(@Path("post_id") String user);
}