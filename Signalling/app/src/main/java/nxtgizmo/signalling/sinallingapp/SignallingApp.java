package nxtgizmo.signalling.sinallingapp;

import android.app.Application;

import nxtgizmo.signalling.BuildConfig;
import nxtgizmo.signalling.di.component.ContactComponent;
import nxtgizmo.signalling.di.component.DaggerContactComponent;
import nxtgizmo.signalling.di.model.AppModule;
import nxtgizmo.signalling.di.model.NetModule;
import timber.log.Timber;

/**
 * Created by 638 on 12/17/2016.
 */

public class SignallingApp extends Application {
    private ContactComponent contactComponent;
    @Override
    public void onCreate() {
        super.onCreate();

        //Initialize Dagger2 component
        contactComponent = DaggerContactComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule("https://jsonplaceholder.typicode.com"))
                .build();

        //Initialize Timber logger
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            //Timber.plant(new CrashReportingTree());
        }
    }

    public ContactComponent getContactComponent(){
        return  contactComponent;
    }
}
